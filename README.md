# sec
Security Resources

## Red Team
- [Mitre: Adversary Emulation Plans](https://attack.mitre.org/resources/adversary-emulation-plans/)

## Blue Team


## Digital Forensics
- https://csilinux.com/
- https://training.csilinux.com/


## Evaluations
- https://attackevals.mitre-engenuity.org/

## Updates (Intelligence)
- http://informationwarfarecenter.com/Cyber_Intelligence_Report.php

## Learning
- https://learn.snyk.io/
- https://tryhackme.com/hacktivities

## CTF
- https://go.snyk.io/ctf101.html


## Misc
- https://www.crowdstrike.com/cybersecurity-101/security-operations-center-soc/
- [Archive of Goodies](https://archive.ph/j49QO)
- [what are edr / xdr / mdr](https://www.forbes.com/sites/forbestechcouncil/2021/04/15/edr-xdr-and-mdr-understanding-the-differences-behind-the-acronyms/?sh=3957aa6549e2)
- [SIEM, SOC, SOAR, XDR Defined](https://www.blumira.com/siem-soc-soar-xdr-defined/)
- [Data Loss Prevention: DLP](https://www.imperva.com/learn/data-security/data-loss-prevention-dlp/)
- [Yara](https://www.varonis.com/blog/yara-rules)
- [Zeek Lang](https://docs.zeek.org/en/v3.0.14/intro/index.html)

## Hardening
- [Linux Distro Hardening](https://www.linkedin.com/pulse/how-hardening-your-linux-distro-claudio-%C5%A1karecki/)
- [9 ways to Harden Your Linux](https://www.linux.com/news/9-ways-harden-your-linux-workstation-after-distro-installation/)

## C2
- https://www.thec2matrix.com/
- https://docs.google.com/spreadsheets/d/1b4mUxa6cDQuTV2BPC6aA-GR4zGZi0ooPYtBe4IgPsSc/edit#gid=0

## Games
- https://play.backdoorsandbreaches.com/
- https://threatgen.com/

## Unsorted
- **https://book.hacktricks.xyz/linux-hardening/linux-privilege-escalation-checklist**
- https://inventory.raw.pm/resources.html
- https://www.thehacker.recipes/
- https://www.ired.team/
- https://doc.lagout.org/rtfm-red-team-field-manual.pdf
- https://www.vulnhub.com/resources/
- https://6odhi.github.io/myarsenal/
- https://github.com/infosecn1nja/Red-Teaming-Toolkit
- https://github.com/foi-oss/ortbot#linux-network-commands
- https://reconshell.com/introduction-to-bash-scripting/
- https://dmcxblue.gitbook.io/red-team-notes-2-0/
- https://www.linode.com/docs/guides/security/
- https://www.redteamsecure.com/research/antivirus-bypass-techniques-with-ntlmrelayx
- https://www.cynet.com/attack-techniques-hands-on/the-art-of-persistence/
- https://www.baeldung.com/linux/silencing-bash-output
- https://gtfobins.github.io/
- https://github.com/0xffsec/handbook
- https://0x1.gitlab.io/pentesting/Red-Teaming-Toolkit/
- https://kwcsec.gitbook.io/the-red-team-handbook/
- https://usacac.army.mil/sites/default/files/documents/ufmcs/The_Red_Team_Handbook.pdf
- https://www.netspi.com/resources/
- https://github.com/yeyintminthuhtut/Awesome-Red-Teaming
- https://byodinsbeardrpg.itch.io/levelling-up-defences
- https://www.offensive-security.com/learn/
- 
